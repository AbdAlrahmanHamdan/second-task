$(document).ready(function()
{
    getAllUsersData();
});

function getAllUsersData()
{
    $.ajax({
        type: "GET",
        url: "http://dummy.restapiexample.com/api/v1/employees",
        dataType:'JSON',
        success: function (response)
        {
            var table = $("#main_table");
            var data = response["data"];
            for (var i = 0; i < data.length; i++)
            {
                var element = data[i];
                var row = "<tr>";

                var ID = "<td>" +  element["id"] + "</td>";

                var employeeName = "<td>" +  element["employee_name"] + "</td>";

                var employeeSalary = "<td>" + element["employee_salary"] + "</td>";

                var employeeAge = "<td>" + element["employee_age"] + "</td>";

                var profileImage = "<td>" + element["profile_image"] + "</td>";

                var deleteClick = "<button id = " + element["id"] + ">" +
                                  "<img src = " + "../deleteIcon.png>"+ "</button>";

                var deleteUser = "<td>" + deleteClick + "</td>";
                deleteUser.onclick = deleteUserFunction(element["id"]);

                row += ID + employeeName + employeeSalary + employeeAge + profileImage + deleteUser + "</tr>";

                table.append(row);
            }
        }
    });
}

function deleteUserFunction(Id)
{
    console.log(Id);
    $.ajax({
        type: "DELETE",
        url: "http://dummy.restapiexample.com/api/v1/delete/" + Id,
        dataType:'JSON'
    });
}